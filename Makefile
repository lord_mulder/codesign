SHELL := bash

# ---------------------------------------------------------------------------
# FLAGS
# ---------------------------------------------------------------------------

MACHINE := $(shell $(CC) -dumpmachine)
ifeq ($(MACHINE),$(filter x86_64-%,$(MACHINE)))
  MYCPU := x64
  MARCH := x86-64
  MTUNE := znver3
else
  MYCPU := x86
  MARCH := i486
  MTUNE := intel
endif

CFLAGS = -march=$(MARCH) -mtune=$(MTUNE) -Os -DNDEBUG -Wall -flto -Ideps/$(MYCPU)/include
LDFLAGS = -Ldeps/$(MYCPU)/lib -static -Wl,--strip-all -Wl,--trace
LIBS = -lcrypto

ifeq ($(MACHINE),$(filter %-mingw32,$(MACHINE)))
	SUFFIX = .exe
	LDFLAGS += -municode
endif

ifneq ($(XCFLAGS),)
  CFLAGS += $(XCFLAGS)
endif
ifneq ($(XLDFLAGS),)
  LDFLAGS += $(XLDFLAGS)
endif
ifneq ($(XLIBS),)
  LIBS += $(XLIBS)
endif

# ---------------------------------------------------------------------------
# RULES
# ---------------------------------------------------------------------------

.PHONY: all keygen sign verify subdirs clean

all: keygen sign verify

keygen: subdirs src/.magic.h rsrc
	$(CC) $(CFLAGS) $(LDFLAGS) -o bin/codesign_keygen$(SUFFIX) src/codesign_keygen.c src/common.c obj/version_keygen.o $(LIBS)

sign: subdirs src/.magic.h
	$(CC) $(CFLAGS) $(LDFLAGS) -o bin/codesign_sign$(SUFFIX) src/codesign_sign.c src/common.c obj/version_sign.o $(LIBS)

verify: subdirs src/.magic.h rsrc
	$(CC) $(CFLAGS) -UEMBED_PUBKEY $(LDFLAGS) -o bin/codesign_verify$(SUFFIX) src/codesign_verify.c src/common.c obj/version_verify.o $(LIBS)
	$(CC) $(CFLAGS) -DEMBED_PUBKEY $(LDFLAGS) -o bin/codesign_verifz$(SUFFIX) src/codesign_verify.c src/common.c obj/version_verify.o $(LIBS)

src/.magic.h:
	str=$$(tr -dc '0-9A-F' < /dev/urandom | head -c 26); \
	printf 'static const unsigned char MAGIC_NMBR[] = { ' > $@; \
	for i in {0..12}; do \
		[ $$i -gt 0 ] && printf ', '; \
		printf '0x%s' $${str:((2*i)):2}; \
	done >> $@; \
	printf ' };\n' >> $@

rsrc: subdirs
ifeq ($(MACHINE),$(filter %-mingw32,$(MACHINE)))
	windres -DAPP="Key Generator ($(MYCPU))" -DNAME=keygen -o obj/version_keygen.o res/version.rc
	windres -DAPP="Signer ($(MYCPU))" -DNAME=sign -o obj/version_sign.o res/version.rc
	windres -DAPP="Verifier ($(MYCPU))" -DNAME=verify -o obj/version_verify.o res/version.rc
else
	$(CC) -o obj/version_keygen.o -xc -c - < /dev/null
	$(CC) -o obj/version_sign.o -xc -c - < /dev/null
	$(CC) -o obj/version_verify.o -xc -c - < /dev/null
endif

subdirs: deps/$(MYCPU)/lib/libcrypto.a deps/$(MYCPU)/lib/libssl.a
	@mkdir -p bin obj

clean:
	rm -rf bin obj
	rm -f src/.magic.h
