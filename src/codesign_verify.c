/******************************************************************************/
/* CodeSign, by LoRd_MuldeR <MuldeR2@GMX.de>                                  */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#include "common.h"
#include ".magic.h"

#include <string.h>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/err.h>

#ifdef EMBED_PUBKEY
#define ARGV_INPUTFILE argv[1]
#define ARGV_SIGNATURE argv[2]
#else
#define ARGV_INPUTFILE argv[2]
#define ARGV_SIGNATURE argv[3]
#endif

#define BUFFSIZE 4096

int MAIN(int argc, CHAR_T *argv[])
{
    int exit_code = EXIT_FAILURE;
    BIO *bio_pubkey = NULL;
    RSA *rsa = NULL;
    SHA512_CTX sha512 = { };
    char *base64 = NULL;
    FILE *file_pubkey = NULL, *file_data = NULL, *file_signature = NULL;
    unsigned char buffer[BUFFSIZE], digest[SHA512_DIGEST_LENGTH], *source = NULL;
    unsigned int source_length = 0U, base64_length = 0U;
#ifdef EMBED_PUBKEY
    const unsigned char *public_key = NULL, *checksum_pubkey = NULL;
    unsigned int pubkey_length = 0U, checksum_length = 0U;
#endif

    /*-------------------------------------------------------*/
    /* Check arguments                                       */
    /*-------------------------------------------------------*/

    print_logo("File Verifier");

#ifdef EMBED_PUBKEY

    if ((argc < 3) || (!argv[1][0]) || (!argv[2][0]))
    {
        print_license();
        fputs("Usage:\n", stderr);
        fputs("  codesign_verifz.exe <filename.dat> <signature.sig>\n\n", stderr);
        goto clean_up;
    }

#else

    if ((argc < 4) || (!argv[1][0]) || (!argv[2][0]) || (!argv[3][0]))
    {
        print_license();
        fputs("Usage:\n", stderr);
        fputs("  codesign_verify.exe <signkey.pub> <filename.dat> <signature.sig>\n\n", stderr);
        goto clean_up;
    }

#endif

    /*-------------------------------------------------------*/
    /* Load the public key                                   */
    /*-------------------------------------------------------*/

#ifdef EMBED_PUBKEY

    public_key = load_resource_data(L"RSA_PUBLIC_KEY", &pubkey_length);
    if (!public_key)
    {
        fputs("Error: Failed to load public key data from resources!\n\n", stderr);
        goto clean_up;
    }

    checksum_pubkey = load_resource_data(L"CHECKSUM_SHA512", &checksum_length);
    if ((!checksum_pubkey) || (checksum_length < SHA512_DIGEST_LENGTH))
    {
        fputs("Error: Failed to load public key checksum from resources!\n\n", stderr);
        goto clean_up;
    }

    if (!SHA512(public_key, pubkey_length, digest))
    {
        fputs("Error: Failed to compute checksum of the public key!\n\n", stderr);
        goto clean_up;
    }

    if (memcmp(digest, checksum_pubkey, SHA512_DIGEST_LENGTH) != 0)
    {
        fputs("Error: Public key checksum does not match the expected value!\n\n", stderr);
        goto clean_up;
    }

    bio_pubkey = BIO_new_mem_buf(public_key, pubkey_length);
    if (!bio_pubkey)
    {
        fputs("Error: Failed to allocate memory BIO for public key!\n\n", stderr);
        goto clean_up;
    }

    if (!PEM_read_bio_RSAPublicKey(bio_pubkey, &rsa, NULL, NULL))
    {
        fputs("Error: Failed to read the public key from memory!\n\n", stderr);
        goto clean_up;
    }

#else

    file_pubkey = FOPEN_RD(argv[1]);
    if (!file_pubkey)
    {
        fputs("Error: Failed to open public key input file!\n\n", stderr);
        goto clean_up;
    }

    if (!PEM_read_RSAPublicKey(file_pubkey, &rsa, NULL, NULL))
    {
        fputs("Error: Failed to read the public key from file!\n\n", stderr);
        goto clean_up;
    }

#endif

    if (RSA_size(rsa) < RSAKEY_MINLEN)
    {
        fputs("Error: RSA key size is smaller than the required minimum!\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Open input files                                      */
    /*-------------------------------------------------------*/

    file_data = FOPEN_RD(ARGV_INPUTFILE);
    if (!file_data)
    {
        fputs("Error: Failed to open the input file!\n\n", stderr);
        goto clean_up;
    }

    file_signature = FOPEN_RD(ARGV_SIGNATURE);
    if (!file_signature)
    {
        fputs("Error: Failed to open the signature file!\n\n", stderr);
        goto clean_up;
    }

    base64 = read_line_from_file(file_signature, 1);
    if (!base64)
    {
        fputs("Error: Failed to read the signature from file!\n\n", stderr);
        goto clean_up;
    }

    if ((base64_length = strlen(base64)) < 4U)
    {
        fputs("Error: The signature appears to be incomplete!\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Decode signature input data                           */
    /*-------------------------------------------------------*/

    source = (unsigned char*) OPENSSL_zalloc(1U + (3U * ((base64_length + 3U) / 4U)));
    if (!source)
    {
        fputs("Error: Failed to allocate the input buffer!\n\n", stderr);
        goto clean_up;
    }

    source_length = int2uint(EVP_DecodeBlock(source, (unsigned char*)base64, base64_length));
    if (!(source_length > 0U))
    {
        fputs("Error: Failed to decode signature data from Base64 format!\n\n", stderr);
        goto clean_up;
    }

    if (source_length <= sizeof(uint64_t))
    {
        fputs("Error: Signature binary data appears to be truncated!\n\n", stderr);
        goto clean_up;
    }

    while ((base64_length > 0U) && (source_length > sizeof(uint64_t) + 1U))
    {
        if (base64[base64_length - 1U] != '=')
        {
            break;
        }
        --source_length;
        --base64_length;
    }

    /*-------------------------------------------------------*/
    /* Compute file digest                                   */
    /*-------------------------------------------------------*/

    if (SHA512_Init(&sha512) != 1)
    {
        fputs("Error: Failed to initialize SHA-512 digest!\n\n", stderr);
        goto clean_up;
    }

    fputs("Verifying the RSA signature, please wait...\n", stderr);
    fflush(stderr);

    if (SHA512_Update(&sha512, source, sizeof(uint64_t)) != 1)
    {
        fputs("Failed!\n\nError: Failed to update SHA-512 digest!\n\n", stderr);
        goto clean_up;
    }

    while (!(feof(file_data) || ferror(file_data)))
    {
        const size_t count = fread(buffer, sizeof(unsigned char), BUFFSIZE, file_data);
        if (count > 0U)
        {
            if (SHA512_Update(&sha512, buffer, count) != 1)
            {
                fputs("Failed!\n\nError: Failed to update SHA-512 digest!\n\n", stderr);
                goto clean_up;
            }
        }
    }

    if (ferror(file_data))
    {
        fputs("Failed!\n\nI/O Error: Failed to read input file!\n\n", stderr);
        goto clean_up;
    }

    if (SHA512_Update(&sha512, MAGIC_NMBR, sizeof(MAGIC_NMBR)) != 1)
    {
        fputs("Failed!\n\nError: Failed to update SHA-512 digest!\n\n", stderr);
        goto clean_up;
    }

    if (SHA512_Final(digest, &sha512) != 1)
    {
        fputs("Failed!\n\nError: Failed to finalize SHA-512 digest!\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Validate the RSA signature                            */
    /*-------------------------------------------------------*/

    if (RSA_verify(NID_sha512, digest, SHA512_DIGEST_LENGTH, source + sizeof(uint64_t), source_length - sizeof(uint64_t), rsa) != 1)
    {
        fputs("Failed!\n\nInvalid signature or corrupted file :-(\n\n", stderr);
        goto clean_up;
    }

    fputs("Completed.\n\nThe signature is valid :-)\n\n", stderr);
    exit_code = EXIT_SUCCESS;

    /*-------------------------------------------------------*/
    /* Final clean-up                                        */
    /*-------------------------------------------------------*/

clean_up:

    OPENSSL_clear_free(source, source_length);
    OPENSSL_clear_free(base64, base64_length);

    if (file_pubkey)
    {
       fclose(file_pubkey);
    }

    if (file_data)
    {
        fclose(file_data);
    }

    if (file_signature)
    {
        fclose(file_signature);
    }

    if (bio_pubkey)
    {
        BIO_free(bio_pubkey);
    }

    if (rsa)
    {
        RSA_free(rsa);
    }

    OPENSSL_cleanse(&sha512, sizeof(SHA512_CTX));

    return exit_code;
}
