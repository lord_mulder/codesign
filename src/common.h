/******************************************************************************/
/* CodeSign, by LoRd_MuldeR <MuldeR2@GMX.de>                                  */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef _COMMON_H
#define _COMMON_H

#include "platform.h"
#include <stdint.h>

#define PASSWD_MINLEN 8
#define RSAKEY_MINLEN 1920

void print_logo(const char *const app_name);
void print_license(void);
unsigned int int2uint(const int value);
char *convert_CHAR_to_UTF8(const CHAR_T *str);
char *read_line_from_file(FILE *const file, const int trim);
uint64_t get_current_time_usec(void);
void store_uint64(unsigned char *const buffer, const uint64_t value);
uint64_t load_uint64(const unsigned char *const buffer);
int force_flush(FILE *const file);

#ifdef _WIN32
const unsigned char *load_resource_data(const wchar_t *const name, unsigned int *const length);
#else
#define load_resource_data(X,Y) ((const unsigned char*)NULL)
#endif

#endif /*_COMMON_H*/
