/******************************************************************************/
/* CodeSign, by LoRd_MuldeR <MuldeR2@GMX.de>                                  */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#include "common.h"
#include ".magic.h"

#include <string.h>

#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/evp.h>

#define BUFFSIZE 4096

int MAIN(int argc, CHAR_T *argv[])
{
    int exit_code = EXIT_FAILURE;
    RSA *rsa = NULL;
    SHA512_CTX sha512 = { };
    FILE *file_privkey = NULL, *file_data = NULL, *file_signature = NULL;
    char *passwd = NULL, *base64 = NULL;
    uint64_t timestamp = 0ULL;
    unsigned char buffer[BUFFSIZE], digest[SHA512_DIGEST_LENGTH], ts_buffer[sizeof(uint64_t)], *output = NULL;
    unsigned int signature_length = 0U, output_length = 0U, base64_length = 0U;

    /*-------------------------------------------------------*/
    /* Check arguments                                       */
    /*-------------------------------------------------------*/

    print_logo("File Signer");

    if ((argc < 5) || (!argv[1][0]) || (!argv[2][0]) || (!argv[3][0]) || (!argv[4][0]))
    {
        print_license();
        fputs("Usage:\n", stderr);
        fputs("  codesign_sign.exe <passwd> <signkey.key> <filename.dat> <signature.sig>\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Open input/output files                               */
    /*-------------------------------------------------------*/

    file_privkey = FOPEN_RD(argv[2]);
    if (!file_privkey)
    {
        fputs("Error: Failed to open private key file!\n\n", stderr);
        goto clean_up;
    }

    file_data = FOPEN_RD(argv[3]);
    if (!file_data)
    {
        fputs("Error: Failed to open input file to be signed!\n\n", stderr);
        goto clean_up;
    }

    file_signature = FOPEN_WR(argv[4]);
    if (!file_signature)
    {
        fputs("Error: Failed to open output file for signature!\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Read password                                         */
    /*-------------------------------------------------------*/

    if ((argv[1][0] == L'-') && (argv[1][1] == L'\0'))
    {
        passwd = read_line_from_file(stdin, 0);
        if (!passwd)
        {
            fputs("Error: Failed to read password from STDIN stream!\n\n", stderr);
            goto clean_up;
        }
    }
    else
    {
        passwd = convert_CHAR_to_UTF8(argv[1]);
        if (!passwd)
        {
            fputs("Error: Failed to convert password to UTF-8 format!\n\n", stderr);
            goto clean_up;
        }
    }

    if (strlen(passwd) < PASSWD_MINLEN)
    {
        fprintf(stderr, "Error: Password is too short! (min. length: %d)\n\n", PASSWD_MINLEN);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Read the private key                                  */
    /*-------------------------------------------------------*/

    if (!PEM_read_RSAPrivateKey(file_privkey, &rsa, NULL, passwd))
    {
        fputs("Error: Failed to read the private key! Wrong password?\n\n", stderr);
        goto clean_up;
    }

    if (RSA_size(rsa) < RSAKEY_MINLEN)
    {
        fputs("Error: RSA key size is smaller than the required minimum!\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Get current time                                      */
    /*-------------------------------------------------------*/

    timestamp = get_current_time_usec();
    if (!(timestamp > 0ULL))
    {
        fputs("Error: Failed to get the current system time!\n\n", stderr);
        goto clean_up;
    }

    store_uint64(ts_buffer, timestamp);

    /*-------------------------------------------------------*/
    /* Compute file digest                                   */
    /*-------------------------------------------------------*/

    if (SHA512_Init(&sha512) != 1)
    {
        fputs("Error: Failed to initialize SHA-512 digest!\n\n", stderr);
        goto clean_up;
    }

    fputs("Generating the RSA signature, please wait...\n", stderr);
    fflush(stderr);

    if (SHA512_Update(&sha512, ts_buffer, sizeof(ts_buffer)) != 1)
    {
        fputs("Failed!\n\nError: Failed to update SHA-512 digest!\n\n", stderr);
        goto clean_up;
    }

    while (!(feof(file_data) || ferror(file_data)))
    {
        const size_t count = fread(buffer, sizeof(unsigned char), BUFFSIZE, file_data);
        if (count > 0U)
        {
            if (SHA512_Update(&sha512, buffer, count) != 1)
            {
                fputs("Failed!\n\nError: Failed to update SHA-512 digest!\n\n", stderr);
                goto clean_up;
            }
        }
    }

    if (ferror(file_data))
    {
        fputs("Failed!\n\nI/O Error: Failed to read input file!\n\n", stderr);
        goto clean_up;
    }

    if (SHA512_Update(&sha512, MAGIC_NMBR, sizeof(MAGIC_NMBR)) != 1)
    {
        fputs("Failed!\n\nError: Failed to update SHA-512 digest!\n\n", stderr);
        goto clean_up;
    }

    if (SHA512_Final(digest, &sha512) != 1)
    {
        fputs("Failed!\n\nError: Failed to finalize SHA-512 digest!\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Compute the RSA signature                             */
    /*-------------------------------------------------------*/

    output = (unsigned char*) OPENSSL_zalloc(RSA_size(rsa) + sizeof(ts_buffer));
    if (!output)
    {
        fputs("Failed!\n\nError: Failed to allocate output buffer!\n\n", stderr);
        goto clean_up;
    }

    memcpy(output, ts_buffer, sizeof(ts_buffer));

    if (RSA_sign(NID_sha512, digest, SHA512_DIGEST_LENGTH, output + sizeof(ts_buffer), &signature_length, rsa) != 1)
    {
        fputs("Failed!\n\nError: Failed to compute signature!\n\n", stderr);
        goto clean_up;
    }

    fputs("Completed.\n\n", stderr);
    fflush(stderr);

    /*-------------------------------------------------------*/
    /* Write signature the output file                       */
    /*-------------------------------------------------------*/

    base64 = (char*) OPENSSL_zalloc(1U + (base64_length = 4U * (((output_length = signature_length + sizeof(ts_buffer)) + 2U) / 3U)));
    if (!base64)
    {
        fputs("Error: Failed to allocate hex-string buffer!\n\n", stderr);
        goto clean_up;
    }

    if (int2uint(EVP_EncodeBlock((unsigned char*)base64, output, output_length)) != base64_length)
    {
        fputs("Error: Failed to encode signature to Base64!\n\n", stderr);
        goto clean_up;
    }

    if (fwrite(base64, sizeof(char), base64_length, file_signature) != base64_length)
    {
        fputs("Error: Failed to write signature to output file!\n\n", stderr);
        goto clean_up;
    }

    if (!force_flush(file_signature))
    {
        fputs("I/O Error: Signature could not be written completely!\n\n", stderr);
        goto clean_up;
    }

    fputs("RSA signature written successfully.\n\n", stderr);
    exit_code = EXIT_SUCCESS;

    /*-------------------------------------------------------*/
    /* Final clean-up                                        */
    /*-------------------------------------------------------*/

clean_up:

    OPENSSL_clear_free(base64, base64_length);
    OPENSSL_clear_free(output, output_length);

    if (passwd)
    {
        OPENSSL_clear_free(passwd, strlen(passwd));
    }

    if (file_privkey)
    {
        fclose(file_privkey);
    }

    if (file_data)
    {
        fclose(file_data);
    }

    if (file_signature)
    {
        fclose(file_signature);
    }

    if (rsa)
    {
        RSA_free(rsa);
    }

    OPENSSL_cleanse(ts_buffer, sizeof(ts_buffer));
    OPENSSL_cleanse(&sha512, sizeof(SHA512_CTX));
    OPENSSL_cleanse(&timestamp, sizeof(uint64_t));

    return exit_code;
}
