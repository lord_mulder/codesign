#ifndef _PLATFORM_H
#define _PLATFORM_H

#include "platform.h"
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>

#ifdef _WIN32
#include <share.h>

#define CHAR_T wchar_t
#define MAIN wmain
#define FOPEN_RD(X) _wfsopen((X), L"rb", _SH_DENYWR)
#define FOPEN_WR(X) _wfsopen((X), L"wb", _SH_DENYRW)

#else

#define CHAR_T char
#define MAIN main
#define FOPEN_RD(X) fopen((X), "rb")
#define FOPEN_WR(X) fopen((X), "wb")

#endif

#endif /*_PLATFORM_H*/
