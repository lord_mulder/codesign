---
title: "![CodeSign](etc/img/Logo.png)"
---


Introduction
============

**CodeSign** is a simple file signing and verification toolkit, based on cryptographic routines from the Libcrypto (OpenSSL) library.

At this time, CodeSign employs the RSA signature algorithm with 15360-Bit keys and the SHA-512 hash function.

This software was created by LoRd_MuldeR [&lt;MuldeR2@GMX.de&gt;](mailto:MuldeR2@GMX.de). Please see <http://www.muldersoft.com> for additional information.


Synopsis
========

The CodeSign suite is composed of the following tools:

Key Generator
-------------

The **key generator** tool is used to generate a new key-pair. It creates the *public key* and the *private key* file. The private key will be protected by the specified *password*.

```
Usage:
  codesign_keygen.exe <passwd> <signkey.pub> <signkey.key>
```

File Signer
-----------

The **file signer** tool is used to create a new signature. It creates the *signature* file for the specified *input* (original) file, using the existing *private key* protected by *password*.

```
Usage:
  codesign_sign.exe <passwd> <signkey.key> <filename.dat> <signature.sig>
```

File Verifier
-------------

The **file verifier** tool is used to check whether the specified *signature* file is valid *and* matches the given (possibly tampered) *input* file, using the specified *public key*.

```
Usage:
  codesign_verify.exe <signkey.pub> <filename.dat> <signature.sig>
```

There is an alternative variant of the *file verifier* tool that uses an "embedded" public key (from the file's resources) and therefore does ***not*** need a separate *public key* file.

```
Usage:
  codesign_verifz.exe <filename.dat> <signature.sig>
```


Embedding the Public Key
========================

In order to use the **file verifier** tool with an *embedded* public key (`codesign_verifz.exe`), the *public key* must be added to the file's resource section first:

* It is recommended to use the [Resource Hacker&trade;](http://www.angusj.com/resourcehacker/) utility for adding resources to the executable file. Use command *"Action"* &rarr; *"Add Image or Other Binary Resource"*.

* The *public* RSA key, in OpenSSL PEM format, shall be embedded as a resource of type `RT_RCDATA` with the name **`RSA_PUBLIC_KEY`**.

* The SHA-512 digest of the *public* RSA key, in "raw" (binary) format, shall be embedded as a resource of type `RT_RCDATA` with the name **`CHECKSUM_SHA512`**.

  OpenSSL command to generate the required SHA-512 digest:
  ```
  openssl.exe dgst -sha512 -binary -out signkey.pub.sha512 signkey.pub
  ```


Quick Start Guide
=================

1. First of all, generate your RSA key-pair, if not already done:

   ```
   codesign_keygen.exe your-password-here signkey.pub signkey.key
   ```

   **Note:** This step is required only *once*. The same key-pair can be used to sign *many* files. Keep the *private* key (and the password) confidential!

2. Sign the file to be distributed, using the previously generated private key:

   ```
   codesign_sign.exe your-password-here signkey.key my-installer.exe my-installer.exe.sig
   ```

   **Note:** The generated signature file shall be distributed alongside with the signed file. The *private key* ***must not*** be distributed under any circumstances!

3. Verify the retrieved (e.g. downloaded) file:

   ```
   codesign_verify.exe signkey.pub my-installer.exe my-installer.exe.sig
   ```

   **Note:** This step is used to verify that the retrieved file was signed by the original author and that it was ***not*** tampered with since it was signed.


Passing the Password
====================

Passing the password directly on the command-line is **not** recommended for security reasons!

The password can be passed to the CodeSign tools via pipe instead. Setting the password to `-` will cause the program to read the password from the standard input stream:

* For example, the password can be read from a password file:

  ```
  codesign_keygen.exe - signkey.pub signkey.key < password.txt
  ```

* Alternatively, the **`passentry.exe`** helper application can be used to display a simple passwort entry dialog to the user:

  ```
  passentry.exe | codesign_keygen.exe - signkey.pub signkey.key
  ```


License
=======

This work has been released under the **CC0 1.0 Universal** license.

For details, please refer to:  
<https://creativecommons.org/publicdomain/zero/1.0/legalcode>
